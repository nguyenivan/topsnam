USE topsnam;
DROP TABLE IF EXISTS frontend_vote;
ALTER TABLE frontend_top ADD vote_count integer DEFAULT 0 NOT NULL;
ALTER TABLE frontend_top ADD local_name varchar(256);

CREATE TABLE `frontend_top_vote` (
    `id` integer AUTO_INCREMENT NOT NULL PRIMARY KEY,
    `top_id` integer NOT NULL,
    `user_id` integer NOT NULL,
    UNIQUE (`top_id`, `user_id`)
)
;
ALTER TABLE `frontend_top_vote` ADD CONSTRAINT `user_id_refs_id_186bed9a` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);
ALTER TABLE `frontend_top_vote` ADD CONSTRAINT `top_id_refs_id_21acfc51` FOREIGN KEY (`top_id`) REFERENCES `frontend_top` (`id`);
