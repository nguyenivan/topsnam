#!/bin/sh
src="/Users/nguyenivan/Projects/topsnam"
dst="/Users/nguyenivan/Deploys/topsnam"
manage=$src"/manage.py"
echo "Collecting static files..."
python2.7 $manage collectstatic --verbosity=0 --noinput
echo "Syncing directory tree..."
rsync --recursive --update --delete --delete-excluded --times --exclude="*/static" --exclude="*.sh" --exclude="*.pyc" --exclude="*.log" --exclude=".*" --exclude="site_settings.py" $src/* $dst
echo "Removing built media folder in destination tree..."
rm -rf $dst"/media/"
echo "Updating Google App Engine..."
python2.7 /usr/local/bin/appcfg.py --oauth2 update $dst
echo "Done!"

