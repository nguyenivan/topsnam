# -*- coding: utf-8 -*-
from django.shortcuts import  get_object_or_404, redirect, render
from frontend.models import Top, Location
#from django.contrib.auth.decorators import login_required
from django.contrib.messages.api import get_messages
from social_auth.views import setting
from django.contrib.auth import logout
	
def home(request):
	template_name = "frontend/top_list.html"
	data = { 
			'top_list': Top.get_sorted(),
			}
	return render(request, template_name, data)

def local(request):
	path = '/%s/' %  '/'.join([t for t in request.path.lower().split('/') if t])
	location = get_object_or_404(Location, path=path)
	template_name = "frontend/local.html"
	data = {'location': location}
	return render(request, template_name, data)

def accountscreateuser(request):
	name = setting('SOCIAL_AUTH_PARTIAL_PIPELINE_KEY', 'partial_pipeline')
	if request.method == 'POST' and request.POST.get('username'):
		request.session['saved_username'] = request.POST['username']
		avatar_file = request.FILES.get('avatar_file')
		if avatar_file:
			from django.core.files.base import ContentFile	
			from django.core.files.storage import default_storage
			avatar_url = default_storage.save(avatar_file.name, ContentFile(avatar_file.read())) 
		else:
			avatar_url = request.POST.get('avatar_url') 
		request.session['avatar_url'] = avatar_url
		backend = request.session[name]['backend']
		return redirect('socialauth_complete', backend=backend)
	else:
		response = request.session[name].get('kwargs').get('response')
		authid = response.get('id')
		if authid:
			backend = request.session[name].get('backend')
			if backend == 'facebook':
				avatar = "http://graph.facebook.com/%s/picture?type=large" % response["id"]
			elif backend == 'google':
				avatar = response.get("picture")
			elif backend == 'twitter':
				avatar = response.get("profile_image_url")
		return render(request, 'frontend/accounts/createuser.html', {'avatar': avatar} )

def accountserror(request):
	messages = get_messages(request)
	print messages, dir(messages)
	return render(request, 'frontend/accounts/error.html', {'messages': messages})

def accountslogout(request):
	"""Logs out user"""
	logout(request)
	return redirect('frontend_home')

