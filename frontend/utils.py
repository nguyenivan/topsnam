# -*- coding: utf-8 -*-
import urlparse
import re
from exceptions import Exception, ImportError
from cStringIO import StringIO
import unidecode


class PageNotFoundException(Exception):
	pass
try:
	from settings import USE_PYCURL
except ImportError:
	USE_PYCURL = False
from lxml.html import parse	
if USE_PYCURL:
	from pycurl import Curl
	c = Curl()
	c.setopt(c.CONNECTTIMEOUT, 5)
	c.setopt(c.TIMEOUT, 5)
	c.setopt(c.SSL_VERIFYPEER, False)
	c.setopt(c.SSL_VERIFYHOST, False)
	c.setopt(c.NOPROGRESS, 1)
	def get_thumb_list(url):
		buf = StringIO()
		try:
			c.setopt(c.URL, str(url))
			c.setopt(c.NOBODY, False)
			c.setopt(c.WRITEFUNCTION, buf.write)
			c.perform()
			soup = BeautifulSoup(buf.getvalue())
		except Exception as e:
			buf.close()
			raise e
		# get all non gif, none png images
		img_list = soup.find_all('img', src=re.compile('^.+(?<!gif|png)$'))
		result = []
		for img in img_list:
			c.setopt(c.NOBODY, True)
			c.setopt(c.WRITEFUNCTION, lambda x: None)
			src = img.get('src')
			if src:
				fullsrc = str(urlparse.urljoin(url, src))
				print fullsrc
				c.setopt(c.URL, fullsrc)
				c.perform()
				if c.getinfo(c.HTTP_CODE) == 200 and 'image' in c.getinfo(c.CONTENT_TYPE):
					result.append( ( c.getinfo(c.CONTENT_LENGTH_DOWNLOAD),fullsrc) )
					
		result = sorted(result, key=lambda url: url[0], reverse=True)
		result = [u[1] for u in result if u[0]>5000]
					
		return result
else:
	import urllib2
	def get_thumb_list(url):
		response = urllib2.urlopen(url)
		if response.getcode()!=200:
			return [] #error image here
		page = parse(response).getroot()
		response.close()
		# get all non gif, none png images
		img_list = page.xpath("//img[not(contains(@src, '.gif')) and not(contains(@src, '.png'))]")
		result = []
		for img in img_list:
			try:
				src = img.get('src')
				if src:
					fullsrc = str(urlparse.urljoin(url, src))
					print fullsrc, response.getcode(), response.headers.get('content-type'), response.headers.get('content-length')
					response = urllib2.urlopen(fullsrc)
					if response.getcode() == 200:
						result.append( ( get_int_value(response.headers,'content-length'),fullsrc) )
					response.close()
			except Exception as e:
				print e
				
		result = sorted(result, key=lambda url: url[0], reverse=True)
		result = [u[1] for u in result if u[0]>5000]
					
		return result
	
from exceptions import ValueError
def get_int_param(request, key, errorvalue=-1):
	return get_int_value(request.REQUEST, key, errorvalue)

def get_int_value(d, key, errorvalue=-1):
	try:
		return int(d.get(key,errorvalue))
	except ValueError:
		return errorvalue

	
def get_youtube_id(url):
	parsed = urlparse.urlparse(url)
	if parsed.netloc != 'www.youtube.com': 
		return None
	return urlparse.parse_qs(parsed.query)['v'][0]


def youtube_thumb_by_id(video_id, size = 'l'):
	if not video_id:
		return None
	if size == 's':
		return "http://img.youtube.com/vi/%s/2.jpg" % video_id
	else:
		return "http://img.youtube.com/vi/%s/0.jpg" % video_id

def get_youtube_thumbnail(url, size = 'l'):
	return youtube_thumb_by_id(get_youtube_id(url), size) 

# -*- coding: utf-8 -*-
import base64, struct, math
# encode(1000001) = 'QUIP'
def encode(n):
	data = struct.pack('<Q', n).rstrip('\x00')
	if len(data) == 0:
		data = '\x00'
	s = base64.urlsafe_b64encode(data).rstrip('=')
	return s
	
# decode('QUIP') = 1000001
def decode(s):
	data = base64.urlsafe_b64decode(s + '==')
	n = struct.unpack('<Q', data + '\x00' * (8 - len(data)))
	return n[0]
	
def upper_vietnamese(s):
	if type(s) == type(u""):
		return s.upper()
	return unicode(s, "utf8").upper().encode("utf8")

VNCHAR = 'ạảãàáâậầấẩẫăắằặẳẵóòọõỏôộổỗồốơờớợởỡéèẻẹẽêếềệểễúùụủũưựữửừứíìịỉĩýỳỷỵỹđ'
VNCHAR += upper_vietnamese(VNCHAR)
INTAB = [ch.encode('utf8') for ch in unicode(VNCHAR, 'utf8')]
OUTTAB = "a"*17 + "o"*17 + "e"*11 + "u"*11 + "i"*5 + "y"*5 + "d"
OUTTAB += OUTTAB.upper()

r = re.compile("|".join(INTAB))
replaces_dict = dict(zip(INTAB, OUTTAB))

def remove_accent(utf8_str):
	if type(utf8_str) == type(''):
		return r.sub(lambda m: replaces_dict[m.group(0)], utf8_str)
	else:
		raise (Exception('Input parameter must be str type.'))

def test_accent():
	print 'Test 1 ' + 'Succeed' if 'ThIEn Ha Vo DiCh' == remove_accent('ThIÊn Hạ Vô ĐịCh') else "Fail"
	print 'Test 2 ' + 'Succeed' if 'THIEN HA VO DICH' == remove_accent('THIÊN HẠ VÔ ĐỊCH') else "Fail"
	
import time
def random_date(start, end, prop, fmt=None):
	"""Get a time at a proportion of a range of two formatted times.
	start and end should be strings specifying times formated in the
	given format (strftime-style), giving an interval [start, end].
	prop specifies how a proportion of the interval to be taken after
	start.  The returned time will be in the specified format.
	"""
	if format is None:
		fmt =  '%m/%d/%Y %I:%M %p'
	stime = time.mktime(time.strptime(start, fmt))
	etime = time.mktime(time.strptime(end, fmt))
	ptime = stime + prop * (etime - stime)
	return time.localtime(ptime)


import urllib
def url_with_querystring(path, **kwargs):
	return path + '?' + urllib.urlencode(kwargs)

def slugify(s):
	s = unidecode.unidecode(s).lower()
	return re.sub(r'\W+','-',s)
