from django.contrib import admin
from frontend.models import  UserProfile, Zoom, Location, Top, Pin, CommentTop, CommentPin  

class LocationAdmin(admin.ModelAdmin):
	list_display = ('name', 'local_name', 'latlng', 'zoom', 'superordinate', 'owner', 'added', 'updated')
admin.site.register(Location, LocationAdmin)

admin.site.register(UserProfile)
admin.site.register(Zoom)
admin.site.register(Top)
admin.site.register(Pin)
admin.site.register(CommentTop)
admin.site.register(CommentPin)
