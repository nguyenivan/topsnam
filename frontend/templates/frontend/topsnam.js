function disable(form){
	$("input[type=submit]", form).attr('disabled', 'disabled');
};
function enable(form){
	$("input[type=submit]", form).removeAttr('disabled');
};
$(document).on('click', '.translate',function(){
	$(this).siblings().andSelf().toggle();
});

$(document).on({
	mouseenter: function(){	
		$(".topit", this).fadeIn();
		$(".pinform", this).css({visibility :"visible"});
	},
	mouseleave: function(){ 
		$(".topit", this).fadeOut();
		pinform = $("form.pinform", this);
		if (!$('input', pinform).val() ) {
			pinform.css({visibility: "hidden"});
		};
	},
}, '.top');

$(document).on('submit','form.pinform', function(){
	var form = $(this);
	disable(form);
	var targetform = form.nextAll('form.imageform').fadeIn();
	$('[name=url]', targetform).val( $('[name=pinurl]', form).val() );
	var wait = $('.wait', targetform); 
	wait.prevAll().remove();
	wait.show();
	$.ajax({
		url: "{% url frontend_pingetthumbs %}",
		type: 'post',
		data: form.serialize(),
		success: function(data){
			wait.hide();
			wait.before(data);
			$(".slides", targetform).responsiveSlides({auto: false,pager: false,nav: true,prevText: "←&nbsp;Prev",nextText: "Next&nbsp;→",maxwidth: "170",controls: "",namespace: "rslides",});
		},
		error: function(jqXHR, textStatus, errorThrown) { alert(errorThrown)},
		complete: function(){enable(form);},
	});
	return false;
});

$(document).on('submit', 'form.imageform', function(){
	var form = $(this);
	disable(form);
	$('[name=image_url]', $(this)).val( $('img:visible', $(this)).attr('src'));
	var crumb = $(this).prevAll('.crumb');
	$('.thumb75.wait', crumb).show();
	$.ajax({
		url: "{% url frontend_pinnew %}",
		type: 'post',
		data: $(this).serialize(),
		success: function(data){
			$('.thumb75.wait', crumb).hide();
			$('.thumb75.wait', crumb).before(data);
			crumb.siblings('form').each(function() {
				   this.reset();
			 });
			crumb.siblings('form').css({visibility :"hidden"});
		},
		error: function(jqXHR, textStatus, errorThrown) { 
			$('.thumb75.wait', crumb).hide();
			alert(errorThrown);
		},
		complete: function(){enable(form);},
	});
	return false;
});

$(document).on('submit','form.topit', function(e){
	e.preventDefault();
	form = $(this);
	disable(form);
	$.ajax({
		url: "{% url frontend_votenew %}",
		type: "post",
		data: form.serialize(),
		success: function(data){
			context = form.parents('.top').hide();
			$('html, body').animate({ scrollTop: 0 }, 'slow', function(){
				context.insertAfter($('.tophead')).slideDown();
				$('.topit').hide();
			});
			form.each(function() {
				   this.reset();
			});
		},
		complete: function(){enable(form);},
	});
	return false;
});		
$(document).on('submit', 'form.write', function(e){
	e.preventDefault();
	form = $(this);
	disable(form);
	$.ajax({
		url: "{% url frontend_commentnew %}",
		type: "post",
		data: form.serialize(),
		success: function(data){
			form.before(data);
			form.each(function() {
				   this.reset();
			});
		},
		complete: function(){enable(form);},
	});
	return false;
});

$(document).on('click','.thumb75 a',function(e) {
	e.preventDefault();
	var url = $(this).attr('href');
	$.ajax({
		url: url,
		type: "get",
		data: {url: url},
		success: function(data){
			$('#modal').html(data);
			$('#modal').modal();
		},
	});
	return false;
});

$(document).on('hide','#modal', function(){
	$('#modal').children().remove();
});

$(document).on('click','.newtop',function(){
	$('.newtopwrap').children().toggle();
});

$(document).on('submit', 'form.newtopform', function(e){
	e.preventDefault();
	var form = $(this); 
	disable(form);
	$.ajax({
		url: "{% url frontend_topnew %}",
		type: 'post',
		data: form.serialize(),
		success: function(data){
			$('.newtopwrap').children().toggle();
			$('.newtopwrap').after(data);
			$('form.newtopform').each(function() {
				this.reset();
			});
		},
		complete: function(){enable(form);},
	});
	return false;
});
$(document).on('click', '.newtopform .close', function(e){
	e.preventDefault();
	$('.newtopwrap').children().toggle();
	$('form.newtopform').each(function() {
		this.reset();
	});
});

$(document).on({
	mouseenter: function(){
		$(this).stop().animate( {width: 100, height :100, margin: -11}, 200); //END FUNCTION
		$(this).addClass('image-popout-shadow');
	},
	mouseleave: function(){ 
		$(this).stop().animate( {width: 75, height :75, margin: 0}, 200); //END FUNCTION
		$(this).removeClass('image-popout-shadow');
	},
}, '.thumb75 a img'); 

$(document).ready(function() {
//	$.support.cors = true;
	$.get('{% url frontend_locationgetall %}', {content_type :'application/json; charset=UTF-8'}, function(data) {
		var redIcon = new google.maps.MarkerImage('{{ STATIC_URL }}images/reddot.png');
		var blueIcon = new google.maps.MarkerImage('{{ STATIC_URL }}images/bluedot.png');
		{% if location %} var toploc = {{ location.pk }}; {% endif %}
		$.each(data, function(i, location){
			var latlng = location.fields.latlng.match(/\d+\.\d+/g);
			if (latlng.length<2) return true;
			var istop = location.pk == toploc;
			$('#map').gmap('addMarker', { 
				'position': location.fields.latlng,
				'icon': istop?redIcon:blueIcon ,
				'title': location.fields.name,
			}).click(function() {
				window.location = location.fields.path;
			});
			if (istop) {
				$('#map').gmap('get', 'map').setZoom(8);
				$('#map').gmap('get', 'map').setCenter(new google.maps.LatLng(parseFloat(latlng[0]), parseFloat(latlng[1])));
			}; 
		});
		if (top == -1) {
			var vncenter = new google.maps.LatLng(16.57, 107.18);
			$('#map').gmap('get', 'map').setZoom(5);
			$('#map').gmap('get', 'map').setCenter(vncenter);
		};
	}).error(function(jqXHR, textStatus, errorThrown) { alert(errorThrown); });

});/*Document.ready*/

/************/
/* PLUG-INs */
/************/

!function( $ ){

	  "use strict"

	 /* MODAL CLASS DEFINITION
	  * ====================== */

	  var Modal = function ( content, options ) {
	    this.options = options
	    this.$element = $(content)
	      .delegate('[data-dismiss="modal"]', 'click.dismiss.modal', $.proxy(this.hide, this))
	  }

	  Modal.prototype = {

	      constructor: Modal

	    , toggle: function () {
	        return this[!this.isShown ? 'show' : 'hide']()
	      }

	    , show: function () {
	        var that = this

	        if (this.isShown) return

	        $('body').addClass('modal-open')

	        this.isShown = true
	        this.$element.trigger('show')

	        escape.call(this)
	        backdrop.call(this, function () {
	          var transition = $.support.transition && that.$element.hasClass('fade')

	          !that.$element.parent().length && that.$element.appendTo(document.body) //don't move modals dom position

	          that.$element
	            .show()

	          if (transition) {
	            that.$element[0].offsetWidth // force reflow
	          }

	          that.$element.addClass('in')

	          transition ?
	            that.$element.one($.support.transition.end, function () { that.$element.trigger('shown') }) :
	            that.$element.trigger('shown')

	        })
	      }

	    , hide: function ( e ) {
	        e && e.preventDefault()

	        if (!this.isShown) return

	        var that = this
	        this.isShown = false

	        $('body').removeClass('modal-open')

	        escape.call(this)

	        this.$element
	          .trigger('hide')
	          .removeClass('in')

	        $.support.transition && this.$element.hasClass('fade') ?
	          hideWithTransition.call(this) :
	          hideModal.call(this)
	      }

	  }


	 /* MODAL PRIVATE METHODS
	  * ===================== */

	  function hideWithTransition() {
	    var that = this
	      , timeout = setTimeout(function () {
	          that.$element.off($.support.transition.end)
	          hideModal.call(that)
	        }, 500)

	    this.$element.one($.support.transition.end, function () {
	      clearTimeout(timeout)
	      hideModal.call(that)
	    })
	  }

	  function hideModal( that ) {
	    this.$element
	      .hide()
	      .trigger('hidden')

	    backdrop.call(this)
	  }

	  function backdrop( callback ) {
	    var that = this
	      , animate = this.$element.hasClass('fade') ? 'fade' : ''

	    if (this.isShown && this.options.backdrop) {
	      var doAnimate = $.support.transition && animate

	      this.$backdrop = $('<div class="modal-backdrop ' + animate + '" />')
	        .appendTo(document.body)

	      if (this.options.backdrop != 'static') {
	        this.$backdrop.click($.proxy(this.hide, this))
	      }

	      if (doAnimate) this.$backdrop[0].offsetWidth // force reflow

	      this.$backdrop.addClass('in')

	      doAnimate ?
	        this.$backdrop.one($.support.transition.end, callback) :
	        callback()

	    } else if (!this.isShown && this.$backdrop) {
	      this.$backdrop.removeClass('in')

	      $.support.transition && this.$element.hasClass('fade')?
	        this.$backdrop.one($.support.transition.end, $.proxy(removeBackdrop, this)) :
	        removeBackdrop.call(this)

	    } else if (callback) {
	      callback()
	    }
	  }

	  function removeBackdrop() {
	    this.$backdrop.remove()
	    this.$backdrop = null
	  }

	  function escape() {
	    var that = this
	    if (this.isShown && this.options.keyboard) {
	      $(document).on('keyup.dismiss.modal', function ( e ) {
	        e.which == 27 && that.hide()
	      })
	    } else if (!this.isShown) {
	      $(document).off('keyup.dismiss.modal')
	    }
	  }


	 /* MODAL PLUGIN DEFINITION
	  * ======================= */

	  $.fn.modal = function ( option ) {
	    return this.each(function () {
	      var $this = $(this)
	        , data = $this.data('modal')
	        , options = $.extend({}, $.fn.modal.defaults, $this.data(), typeof option == 'object' && option)
	      if (!data) $this.data('modal', (data = new Modal(this, options)))
	      if (typeof option == 'string') data[option]()
	      else if (options.show) data.show()
	    })
	  }

	  $.fn.modal.defaults = {
	      backdrop: true
	    , keyboard: true
	    , show: true
	  }

	  $.fn.modal.Constructor = Modal


	 /* MODAL DATA-API
	  * ============== */

	  $(function () {
	    $('body').on('click.modal.data-api', '[data-toggle="modal"]', function ( e ) {
	      var $this = $(this), href
	        , $target = $($this.attr('data-target') || (href = $this.attr('href')) && href.replace(/.*(?=#[^\s]+$)/, '')) //strip for ie7
	        , option = $target.data('modal') ? 'toggle' : $.extend({}, $target.data(), $this.data())

	      e.preventDefault()
	      $target.modal(option)
	    })
	  })

	}( window.jQuery );

	$(document).ajaxSend(function(event, xhr, settings) {
	    function getCookie(name) {
	        var cookieValue = null;
	        if (document.cookie && document.cookie != '') {
	            var cookies = document.cookie.split(';');
	            for (var i = 0; i < cookies.length; i++) {
	                var cookie = jQuery.trim(cookies[i]);
	                // Does this cookie string begin with the name we want?
	                if (cookie.substring(0, name.length + 1) == (name + '=')) {
	                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
	                    break;
	                }
	            }
	        }
	        return cookieValue;
	    }
	    function sameOrigin(url) { 
	        // url could be relative or scheme relative or absolute
	        var host = document.location.host; // host + port
	        var protocol = document.location.protocol;
	        var sr_origin = '//' + host;
	        var origin = protocol + sr_origin;
	        // Allow absolute or scheme relative URLs to same origin
	        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
	            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
	            // or any other URL that isn't scheme relative or absolute i.e relative.
	            !(/^(\/\/|http:|https:).*/.test(url));
	    }
	    function safeMethod(method) {
	        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	    }
	    if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
	        xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
	    }
	});
	$(document).ajaxComplete(function(event, xhr, settings) {
		 if (xhr.status == 278) {
	         window.location.href = xhr.getResponseHeader("Location").replace(/\?.*$/, "?next="+window.location.pathname);
	     }
	});
	(function ($, window, i) {
	  $.fn.responsiveSlides = function (options) {

	    // Default settings
	    var settings = $.extend({
	      "auto": true,             // Boolean: Animate automatically, true or false
	      "speed": 1000,            // Integer: Speed of the transition, in milliseconds
	      "timeout": 4000,          // Integer: Time between slide transitions, in milliseconds
	      "pager": false,           // Boolean: Show pager, true or false
	      "nav": false,             // Boolean: Show navigation, true or false
	      "prevText": "Previous",   // String: Text for the "previous" button
	      "nextText": "Next",       // String: Text for the "next" button
	      "maxwidth": "",           // Integer: Max-width of the slideshow, in pixels
	      "controls": "",           // Selector: Where controls should be appended to, default is after the <ul>
	      "namespace": "rslides"    // String: change the default namespace used
	    }, options);

	    return this.each(function () {

	      // Index for namespacing
	      i++;

	      var $this = $(this),

	        // Local variables
	        selectTab,
	        startCycle,
	        restartCycle,
	        rotate,
	        $tabs,

	        // Helpers
	        index = 0,
	        $slide = $this.children(),
	        length = $slide.size(),
	        fadetime = parseFloat(settings.speed),
	        maxw = parseFloat(settings.maxwidth),

	        // Namespacing
	        namespace = settings.namespace,
	        namespaceIdx = namespace + i,

	        // Classes
	        navClass = namespace + "_nav " + namespaceIdx + "_nav",
	        activeClass = namespace + "_here",
	        visibleClass = namespaceIdx + "_on",
	        slideClassPrefix = namespaceIdx + "_s",

	        // Pager
	        $pager = $("<ul class='" + namespace + "_tabs " + namespaceIdx + "_tabs' />"),

	        // Styles for visible and hidden slides
	        visible = {"float": "left", "position": "relative"},
	        hidden = {"float": "none", "position": "absolute"},

	        // Fading animation
	        slideTo = function (idx) {
	          $this.trigger(namespace + "-before");
	          $slide
	            .stop()
	            .fadeOut(fadetime, function () {
	              $(this)
	                .removeClass(visibleClass)
	                .css(hidden);
	            })
	            .eq(idx)
	            .fadeIn(fadetime, function () {
	              $(this)
	                .addClass(visibleClass)
	                .css(visible)
	                .trigger(namespace + "-after");
	              index = idx;
	            });
	        };

	      // Only run if there's more than one slide
	      if ($slide.size() > 1) {

	        // Add ID's to each slide
	        $slide.each(function (i) {
	          this.id = slideClassPrefix + i;
	        });

	        // Add max-width and classes
	        $this.addClass(namespace + " " + namespaceIdx);
	        if (options && options.maxwidth) {
	          $this.css("max-width", maxw);
	        }

	        // Hide all slides, then show first one
	        $slide
	          .hide()
	          .eq(0)
	          .addClass(visibleClass)
	          .css(visible)
	          .show();

	      }

	      // Navigation
	      if (settings.nav === true) {
	        var navMarkup =
	          "<a href='#' class='" + navClass + " prev'>" + settings.prevText + "</a>" +
	          "<a href='#' class='" + navClass + " next'>" + settings.nextText + "</a>";

	        // Inject navigation
	        if (options.controls) {
	          $(settings.controls).append(navMarkup);
	        } else {
	          $this.after(navMarkup);
	        }

	        var $trigger = $("." + namespaceIdx + "_nav"),
	          $prev = $("." + namespaceIdx + "_nav.prev");

	        // Click event handler
	        $trigger.bind("click", function (e) {
	          e.preventDefault();
	          if ($slide.size() == 1) {
	        	  return;
	          }
	          // Prevent clicking if currently animated
	          if ($("." + visibleClass + ":animated").length) {
	            return;
	          }

	          // Determine where to slide
	          var idx = $slide.index($("." + visibleClass)),
	            prevIdx = idx - 1,
	            nextIdx = idx + 1 < length ? index + 1 : 0;

	          // Go to slide
	          slideTo($(this)[0] === $prev[0] ? prevIdx : nextIdx);
	          if (settings.pager === true) {
	            selectTab($(this)[0] === $prev[0] ? prevIdx : nextIdx);
	          }

	        });
	      }

	      // Max-width fallback
	      if (typeof document.body.style.maxWidth === "undefined" && options && options.maxwidth) {
	        var widthSupport = function () {
	          $this.css("width", "100%");
	          if ($this.width() > maxw) {
	            $this.css("width", maxw);
	          }
	        };

	        // Init fallback
	        widthSupport();
	        $(window).bind("resize", function () {
	          widthSupport();
	        });
	      }

	    });

	  };
	})(jQuery, this, 0);
	
!function ($) {

	  "use strict"; // jshint ;_;


 /* DROPDOWN CLASS DEFINITION
  * ========================= */

  var toggle = '[data-toggle="dropdown"]'
    , Dropdown = function (element) {
        var $el = $(element).on('click.dropdown.data-api', this.toggle)
        $('html').on('click.dropdown.data-api', function () {
          $el.parent().removeClass('open')
        })
      }

  Dropdown.prototype = {

    constructor: Dropdown

  , toggle: function (e) {
      var $this = $(this)
        , $parent
        , selector
        , isActive

      if ($this.is('.disabled, :disabled')) return

      selector = $this.attr('data-target')

      if (!selector) {
        selector = $this.attr('href')
        selector = selector && selector.replace(/.*(?=#[^\s]*$)/, '') //strip for ie7
      }

      $parent = $(selector)
      $parent.length || ($parent = $this.parent())

      isActive = $parent.hasClass('open')

      clearMenus()

      if (!isActive) $parent.toggleClass('open')

      return false
    }

  }

  function clearMenus() {
    $(toggle).parent().removeClass('open')
  }


  /* DROPDOWN PLUGIN DEFINITION
   * ========================== */

  $.fn.dropdown = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('dropdown')
      if (!data) $this.data('dropdown', (data = new Dropdown(this)))
      if (typeof option == 'string') data[option].call($this)
    })
  }

  $.fn.dropdown.Constructor = Dropdown


  /* APPLY TO STANDARD DROPDOWN ELEMENTS
   * =================================== */

  $(function () {
    $('html').on('click.dropdown.data-api', clearMenus)
    $('body')
      .on('click.dropdown', '.dropdown form', function (e) { e.stopPropagation() })
      .on('click.dropdown.data-api', toggle, Dropdown.prototype.toggle)
  })

}(window.jQuery);