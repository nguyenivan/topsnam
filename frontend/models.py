# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from sorl.thumbnail import ImageField
import os
from datetime import datetime, timedelta
from utils import slugify

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	added = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	avatar = ImageField(upload_to='avatar', blank = True, null= True)

class Zoom(models.Model):
	name = models.CharField(max_length=50)
	def __unicode__(self):
		return self.name

def get_default_user():
	qs = User.objects.filter(username='nguyenivan')
	if qs:
		return qs[0]
	else:
		return None

from frontend.utils import remove_accent
import re	

class Location(models.Model):
	name = models.CharField(max_length=100, blank = True)
	local_name = models.CharField(max_length=100)
	latlng = models.CharField(max_length=50)
	zoom = models.ForeignKey(Zoom, default = 2)
	superordinate = models.ForeignKey('self', blank = True, null = True)
	owner = models.ForeignKey(User, default = 1)
	added = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	slug = models.SlugField(blank = True)
	path = models.CharField(max_length=256, blank = True)
	crumb = models.CharField(max_length=512, blank = True)
	def __unicode__(self):
		return self.name
	
	def get_sorted_tops(self):
		last24h = datetime.strftime( datetime.now() - timedelta(days=1), '%Y-%m-%d')
		q1 = Top.objects.filter( updated__gte = last24h, location = self).order_by('-updated')
		q2 = Top.objects.filter( updated__lt = last24h, location = self).order_by('-vote_count')
		return chain (q1, q2)
	
	def save(self, *args, **kwargs):
		if not self.name:
			self.name=remove_accent(self.local_name.encode('utf-8'))
		if not self.slug:
			self.slug =  ''.join(re.findall('\w',self.name)).lower() 
		supers = []
		slugs = []
		count = 0
		loc = self
		while loc and count < 4: #prevent infinite loop
			count+=1
			supers.insert(0,loc)
			slugs.insert(0,loc.slug)
			loc = loc.superordinate
		self.path = '/vietnam/%s/' % '/'.join(slugs)
		links = ['<a href="%s">%s</a>' % ('/', 'Vietnam')]
		for loc in supers:
			links.append('<a href="%s">%s</a>' % (loc.path, loc.name))
		self.crumb = ' > '.join(links)
		super(Location, self).save(*args, **kwargs)
	
	def getfullname(self):
		if not self.local_name:
			return self.name
		else:
			return '%s (%s)' % (self.name, self.local_name)
# Custom manager for this Top class
class TopSortedManager(models.Manager):
	pass

from  itertools import chain	
		
class Top(models.Model):
	name = models.CharField(max_length=256)
	local_name = models.CharField(max_length=256, blank=True, null=True)
	location = models.ForeignKey(Location)
	owner = models.ForeignKey(User, blank = True, related_name = 'tops')
	added = models.DateTimeField(auto_now_add=True)
	vote = models.ManyToManyField(User, related_name = 'vote', blank=True, null=True)
	vote_count = models.IntegerField(default = 0)
	updated = models.DateTimeField(auto_now=True)
	
	def __unicode__(self):
		return self.name
	
	#Get Top List, sort by LTD
	#Get Top List out of 24 hours time frame, sort by #Top, then LTD
	#Limit by 50
	@classmethod
	def get_sorted(cls):
		last24h = datetime.strftime( datetime.now() - timedelta(days=1), '%Y-%m-%d')
		q1 = cls.objects.filter( updated__gte = last24h).order_by('-updated')
		q2 = cls.objects.filter( updated__lt = last24h).order_by('-vote_count')
		return chain (q1, q2)

import urllib2
from StringIO import StringIO
from django.core.files.base import ContentFile
class Pin(models.Model):
	name = models.CharField(max_length=256)
	url = models.URLField()
	image_url = models.URLField()
	top = models.ForeignKey(Top)
	owner = models.ForeignKey(User)
	image_cache = ImageField(upload_to = 'pin', blank = True)
	added = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	
	def save(self, *args, **kwargs):
		if self.image_url:
			input_file = StringIO(urllib2.urlopen(self.image_url).read())
			filename = '%s_%s_%s' % (slugify(self.name), datetime.strftime(datetime.now(),'%h%m%s'), os.path.basename(self.image_url))
			self.image_cache.save(filename, ContentFile(input_file.getvalue()), save=False)
			super(Pin, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.name

class Comment(models.Model):
	content = models.TextField()
	owner = models.ForeignKey(User)
	added = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)

	def __unicode__(self):
		return ( '%s: %s...' % (self.owner.username, (self.content[:17] if len(self.content)>=20 else self.content) ) ) 
	
	class Meta:
		abstract = True

class CommentTop(Comment):
	top = models.ForeignKey(Top)
	
class CommentPin(Comment):
	pin = models.ForeignKey(Pin)
	
