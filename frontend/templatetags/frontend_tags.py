from django import template
from urlparse import parse_qs
import re
register = template.Library()

@register.simple_tag
def active(request, pattern):
	if re.search(pattern, request.path):
		return 'active'
	else:
		return ''

