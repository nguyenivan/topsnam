# -*- coding: utf-8 -*-
from django.http import HttpResponseBadRequest, HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, redirect
from frontend import utils
from frontend.models import Pin , Top, CommentTop, CommentPin
from django.contrib.auth.decorators import login_required
import urlparse

import logging, traceback

def pingetthumbs(request):
	if request.method != 'POST':
		return HttpResponseBadRequest()
	if not request.user.is_authenticated():
		return redirect("frontend_accountslogin")
	pinurl = request.REQUEST.get(u'pinurl')
	if not pinurl: return HttpResponseBadRequest()
	template_name = 'frontend/pinthumbs.htm'
	youtube_thumb = utils.get_youtube_thumbnail(pinurl, 'l')
	if youtube_thumb:
		data = { 'thumbs': [youtube_thumb], 'site': 'youtube'}
	else:
		data = { 'thumbs': utils.get_thumb_list(pinurl)}
	return render_to_response(template_name, data, context_instance=RequestContext(request))

def pinnew(request):
	if request.method != 'POST':
		return HttpResponseBadRequest()	
	if not request.user.is_authenticated():
		return redirect("frontend_accountslogin")	
	topid = utils.get_int_param(request,'topid', -1)
	image_url = request.REQUEST.get('image_url')
	name = request.REQUEST.get('name')
	url = request.REQUEST.get('url')
	owner = request.user
	if not image_url or topid == -1 or not name: return HttpResponseBadRequest()
	top = get_object_or_404(Top, pk=topid)
	pin = Pin(url = url, owner = owner, name = name, image_url = image_url, top = top)
	pin.save()
	template_name = 'frontend/pincrumb.htm'
	data = { 'pin': pin}
	return render_to_response(template_name, data, context_instance=RequestContext(request))

def pingetdetails(request, id):
	pin = get_object_or_404(Pin, pk=id)
	template_name = 'frontend/pindetails.htm'
	parsed = urlparse.urlparse(pin.url)
	if parsed.netloc == 'www.youtube.com':
		data = {
			'site' : 'youtube', 
			'youtube_id' : utils.get_youtube_id(pin.url),
			'pin' : pin,
		}
	else:
		data={'pin':pin} 
	return render_to_response(template_name, data, context_instance=RequestContext(request))

def topnew(request):
	try:
		if request.method != 'POST':
			return HttpResponseBadRequest()	
		if not request.user.is_authenticated():
			return redirect("frontend_accountslogin")
		locationid = utils.get_int_param(request,'locationid', -1)
		name = request.REQUEST.get('name')
		localname = request.REQUEST.get('localname')
		if not name or locationid == -1 : return HttpResponseBadRequest()
		location = get_object_or_404(Location, pk=locationid)
		top = Top(name = name, local_name = localname, owner = request.user, location = location)
		top.save()
		template_name = 'frontend/topboard.htm'
		data = { 'top': top}
		return render_to_response(template_name, data, context_instance=RequestContext(request))
	except:
		print traceback.format_exc()

		

from frontend.models import  Location
from django.core import serializers
def locationgetall(request):
	locations = Location.objects.all()
	data =  serializers.serialize('json', locations, fields=('name', 'latlng', 'path'))	
	return HttpResponse(data, content_type ='application/json; charset=UTF-8')

def commentnew(request):
	if request.method != 'POST':
		return HttpResponseBadRequest()
	if not request.user.is_authenticated():
		return redirect("frontend_accountslogin")
	topid = utils.get_int_param(request,'topid', -1)
	pinid = utils.get_int_param(request,'pinid', -1)
	content = request.REQUEST.get('content')
	owner = request.user
	if not content or not ( (topid > -1) ^ (pinid > -1) ) : return HttpResponseBadRequest()
	if topid > -1:
		top = get_object_or_404(Top, pk=topid)
		comment = CommentTop(content = content, owner = owner, top = top)
	elif pinid > -1: 
		pin = get_object_or_404(Pin, pk=pinid)
		comment = CommentPin(content = content, owner = owner, pin = pin)
	comment.save()
	template_name = 'frontend/comment.htm'
	data = { 'comment': comment}
	return render_to_response(template_name, data, context_instance=RequestContext(request))
		
def votenew(request):
	if request.method != 'POST':
		return HttpResponseBadRequest()
	if not request.user.is_authenticated():
		return redirect("frontend_accountslogin")
	topid = utils.get_int_param(request,'topid', -1)
	if not (topid > -1): return HttpResponseBadRequest()
	user = request.user
	top = get_object_or_404(Top, pk=topid)
	if user not in top.vote.all():
		top.vote.add(user)
		top.vote_count += 1
		top.save()
	return (HttpResponse('success', content_type= 'text/plain; charset=UTF-8' ))
	
