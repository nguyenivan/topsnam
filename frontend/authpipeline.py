from django.shortcuts import redirect
from frontend.models import UserProfile

def create_user_form(*args, **kwargs):
	"""Form for creating new user"""
	if not kwargs['request'].session.get('saved_username') and \
	   kwargs.get('user') is None:
		return redirect( 'frontend_accountscreateuser')

def username(request, *args, **kwargs):
	if kwargs.get('user'):
		username = kwargs['user'].username
	else:
		username = request.session.get('saved_username')
	return {'username': username}

from social_auth.models import User
from social_auth.backends.pipeline import warn_setting	
from social_auth.signals import socialauth_not_registered
from social_auth.utils import setting
from urllib2 import urlopen
from StringIO import StringIO
import os
from django.core.files.base import ContentFile	

	
def create_user(request, backend, details, response, uid, username, user=None, *args,
				**kwargs):
	"""Create user. Depends on get_username pipeline."""
	if user:
		return {'user': user}
	if not username:
		return None

	warn_setting('SOCIAL_AUTH_CREATE_USERS', 'create_user')

	if not setting('SOCIAL_AUTH_CREATE_USERS', True):
		# Send signal for cases where tracking failed registering is useful.
		socialauth_not_registered.send(sender=backend.__class__,
									   uid=uid,
									   response=response,
									   details=details)
		return None

	email = details.get('email')
	newuser = User(username=username, email=email)
	newuser.save()
	avatar_url = request.session.get('avatar_url')
	import traceback
	try:
		if newuser and avatar_url:
			profile = UserProfile(user=newuser)
			input_file = StringIO(urlopen(avatar_url).read())
			basename = os.path.basename(avatar_url)
			if os.path.splitext( basename )[1]:
				filename = '%s_%s' % (newuser.username, basename)
			else:
				filename = '%s_%s.jpg' % (newuser.username, basename)
			profile.avatar.save( filename , ContentFile(input_file.getvalue()), save=False)
			profile.save()
	except :
		print traceback.format_exc()
	return {
		'user': newuser,
		'is_new': True
	}
		
	
