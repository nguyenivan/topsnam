from django.conf.urls.defaults import patterns, url, include
from django.views.generic.simple import direct_to_template

urlpatterns = patterns('frontend.views',
    url(r'^$', 'home', name='frontend_home'),
    url(r'^about/$', direct_to_template, {'template': 'frontend/about.html'}, name='frontend_about'),
    url(r'^contact/$', direct_to_template , {'template': 'frontend/contact.html'}, name='frontend_contact'),
    url(r'^vietnam/', 'local', name='frontend_local'),
    url(r'^testnewpin/$', direct_to_template, {'template':'frontend/testnewpin.html'}),
    url(r'^accounts/error/$', 'accountserror', name='frontend_accountserror'),
    url(r'^accounts/login/$', direct_to_template, {'template': 'frontend/accounts/login.html'}, name = 'frontend_accountslogin'),
    url(r'^accounts/logout/$', 'accountslogout', name = 'frontend_accountslogout'),
    url(r'^accounts/createuser/$', 'accountscreateuser', name='frontend_accountscreateuser'),
    url(r'', include('social_auth.urls')),
)

#    =========================
#    Ajax
#    =========================
urlpatterns += patterns('frontend.ajax',
    url(r'^ajax/pin/getthumbs/$', 'pingetthumbs', name='frontend_pingetthumbs'),
    url(r'^ajax/pin/new/$', 'pinnew', name='frontend_pinnew'),
    url(r'^ajax/pin/getdetails/(?P<id>\d+)/$', 'pingetdetails', name='frontend_pingetdetails'),
    url(r'^ajax/top/new/$', 'topnew', name='frontend_topnew'),
    url(r'^ajax/comment/new/$', 'commentnew', name='frontend_commentnew'),
    url(r'^ajax/location/getall/$', 'locationgetall', name='frontend_locationgetall'),
    url(r'^ajax/vote/new/$', 'votenew', name='frontend_votenew'),
)

