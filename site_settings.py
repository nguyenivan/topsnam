#Invoke SERVER_SOFTWARE='Google App Engine' ./manage.py syncdb to run the local server in production mode
import os

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	# Running on production App Engine, so use a Google Cloud SQL database.
	DATABASES = {
		'default': {
			'ENGINE': 'google.appengine.ext.django.backends.rdbms',
			'INSTANCE': 'qhoach.com:bocation:instance1',
			'NAME': 'bocation',
		}
	}
else:
	DATABASES = {
		'default': {
			'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
			'NAME': 'topsnam',					  # Or path to database file if using sqlite3.
			'USER': 'mysql',					  # Not used with sqlite3.
			'PASSWORD': 'nethouse',				  # Not used with sqlite3.
			'HOST': '',					  # Set to empty string for localhost. Not used with sqlite3.
			'PORT': '',					  # Set to empty string for default. Not used with sqlite3.
		}
	}

if os.getenv('SERVER_SOFTWARE', '').startswith('Google App Engine'):
	CACHES = {
		'default': {
			'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
			'TIMEOUT': 3600*24*2, # Two Weeks
			}
	}
	DEBUG = False
else:
	CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'topsnam',
		}
	}
	DEBUG = True
	
PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
if  os.getenv('SERVER_SOFTWARE', '')  == 'local':
	MEDIA_ROOT = os.path.join(PROJECT_ROOT, "media")
	STATIC_ROOT = os.path.join(PROJECT_ROOT, "static")
	MEDIA_URL = '/media/' 
	STATIC_URL = '/static/'
	ADMIN_MEDIA_PREFIX = '/static/admin/'
else:
	MEDIA_URL = 'http://s3-ap-southeast-1.amazonaws.com/topsnam/media/'
	STATIC_URL = '/static/'
	ADMIN_MEDIA_PREFIX = '/static/admin/'
	FILE_UPLOAD_HANDLERS = ('django.core.files.uploadhandler.MemoryFileUploadHandler',)
	FILE_UPLOAD_MAX_MEMORY_SIZE = 2621440


DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_ACCESS_KEY_ID = 'AKIAIWOS65YTK4ULDR4A'
AWS_SECRET_ACCESS_KEY = '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'
AWS_STORAGE_BUCKET_NAME = 'topsnam'
AWS_S3_CUSTOM_DOMAIN = 's3-ap-southeast-1.amazonaws.com/topsnam'
AWS_LOCATION = 'media'
AWS_QUERYSTRING_AUTH = True
AWS_S3_SECURE_URLS = False

AUTHENTICATION_BACKENDS = (
	'social_auth.backends.facebook.FacebookBackend',
	'django.contrib.auth.backends.ModelBackend',
)
FACEBOOK_APP_ID              = '344565415622816'
FACEBOOK_API_SECRET          = 'e37fa12b2d6b7e5236a0e0fbc13eb2bc'
#FACEBOOK_APP_ID              = '245401445484117'
#FACEBOOK_API_SECRET          = '653c0a95c728ae851c7142bfa8ca2814'
LOGIN_URL          = '/accounts/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_ERROR_URL    = '/accounts/error/'
SOCIAL_AUTH_COMPLETE_URL_NAME  = 'socialauth_complete'
SOCIAL_AUTH_ASSOCIATE_URL_NAME = 'socialauth_associate_complete'

SOCIAL_AUTH_PIPELINE = (
    'social_auth.backends.pipeline.social.social_auth_user',
    'social_auth.backends.pipeline.associate.associate_by_email',
    'social_auth.backends.pipeline.misc.save_status_to_session',
    'frontend.authpipeline.create_user_form',
    'frontend.authpipeline.username',
#    'social_auth.backends.pipeline.user.create_user',
    'frontend.authpipeline.create_user',
    'social_auth.backends.pipeline.social.associate_user',
    'social_auth.backends.pipeline.social.load_extra_data',
    'social_auth.backends.pipeline.user.update_user_details',
)


#SOCIAL_AUTH_NEW_USER_REDIRECT_URL = '/new-users-redirect-url/'
#SOCIAL_AUTH_NEW_ASSOCIATION_REDIRECT_URL = '/new-association-redirect-url/'
#SOCIAL_AUTH_DISCONNECT_REDIRECT_URL = '/account-disconnected-redirect-url/'
#SOCIAL_AUTH_BACKEND_ERROR_URL = '/new-error-url/'